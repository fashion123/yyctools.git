<?php

namespace yyctools\Utils;

class PasswordUtils
{

    /**
     * 检测字符串强度，用于密码检测
     * @param $password 字符串
     * @param $strlent 字符串不小于的长度
     * @param $check_large 是否检测大写
     * @param $check_little 是否检测小写
     * @param $check_num 是否检测数字
     * @param $check_char 是否检测特殊字符
     * @return string|true
     */
    public static  function check_password($password,$strlent = 6,$check_large = true,$check_little = true,$check_num = true,$check_char = true) {
        $pattern1='/[A-Z]/';
        $pattern2='/[a-z]/';
        $pattern3='/[0-9]/';
        $pattern4='/[_.\-#$%]/';

        if(strlen($password)<$strlent) {
            return "密码需要至少包含6个字符";
        }
        if($check_large){
            if(!preg_match($pattern1,$password)) {
                return "密码需要至少一个大写字母";
            }
        }
        if($check_little){
            if(!preg_match($pattern2,$password)) {
                return  "密码需要至少一个小写字母";
            }
        }
        if($check_num){
            if(!preg_match($pattern3,$password)) {
                return "密码需要至少一个数字";
            }
        }
        if($check_char){
            if(!preg_match($pattern4,$password)) {
                return "密码需要至少一个特殊符号：_.-#$%";
            }
        }
       return true;
    }
}