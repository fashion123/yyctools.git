<?php

namespace yyctools\Utils;

class MathUtils
{
    /**
     * 价格精度相加
     * @param $number_es 相加数值数组
     * @param $scale 保留位数
     * @return int|string
     */
    public static function double_add_number(array $number_es,$scale=2){
        $sun_number = 0;
        foreach ($number_es as $val){
            $sun_number = bcadd($sun_number,$val,$scale);
        }
        return $sun_number;
    }

    /**
     *  精度加减
     * @param ...$n 多个值
     * @return int|string
     */
    public static function double_calculation(...$n){
        $num = 0;
        foreach ($n as $i){
            $num = bcadd($num,$i,2);
        }
        return $num;
    }

    /**
     * 求百分比
     * @param $left 除数
     * @param $right 被除数
     * @param $precision 保留小数位数
     * @return float|int
     */
    public static function calculate_percentage($left,$right,$precision=2){
        if($right==0)return 0;
        return round(bcdiv($left * 100,$right,$precision),$precision);
    }

    /**
     * 获取小数点位数
     * @param $float 小数
     * @return int
     */
    public static function get_precision($float){
        $precision_arr =  explode('.',$float);
        if(empty($precision_arr[1]))return 0;
        else return strlen($precision_arr[1]);
    }


    /**
     * 英文数字转中文数字
     * @param $num 数字
     * @return string
     */
    public static function num_to_word($num){
        $chiNum = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
        $chiUni = ['','十', '百', '千', '万', '亿', '十', '百', '千'];
        $chiStr = '';
        $num_str = (string)$num;
        $count = strlen($num_str);
        $last_flag = true; //上一个 是否为0
        $zero_flag = true; //是否第一个
        $temp_num = null; //临时数字
        $chiStr = '';//拼接结果
        if ($count == 2) {//两位数
            $temp_num = $num_str[0];
            $chiStr = $temp_num == 1 ? $chiUni[1] : $chiNum[$temp_num].$chiUni[1];
            //当以1开头 都是十一，十二，以十开头的 我们就取$chiUni[i]也就是十 当不是以1开头时，而是以2,3,4,我们取这个数字相应的中文并拼接上十
            $temp_num = $num_str[1];
            $chiStr .= $temp_num == 0 ? '' : $chiNum[$temp_num];
            //取得第二个值并的到他的中文
        }else if($count > 2){
            $index = 0;
            for ($i=$count-1; $i >= 0 ; $i--) {
                $temp_num = $num_str[$i];         //获取的个位数
                if ($temp_num == 0) {
                    if (!$zero_flag && !$last_flag ) {
                        $chiStr = $chiNum[$temp_num]. $chiStr;
                        $last_flag = true;
                    }
                }else{
                    $chiStr = $chiNum[$temp_num].$chiUni[$index%9] .$chiStr;
                    //$index%9 index原始值为0，所以开头为0 后面根据循环得到：0,1,2,3...（不知道为什么直接用$index而是选择$index%9  毕竟两者结果是一样的）
                    //当输入的值为：1003 ，防止输出一千零零三的错误情况，$last_flag就起到作用了当翻译倒数第二个值时，将$last_flag设定为true;翻译第三值时在if(!$zero&&!$last_flag)的判断中会将其拦截，从而跳过
                    $zero_flag = false;
                    $last_flag = false;
                }
                $index ++;
            }
        }else{
            $chiStr = $chiNum[$num_str[0]]; //单个数字的直接取中文
        }
        return $chiStr;
    }


    /**
     * 整数均分算法
     * @param $number 总数量     10 个苹果
     * @param $total 平均分摊总数  3个人
     * @return false|string[]
     */
    public static function get_divide_number($number, $total) {
        $divide_number  = bcdiv($number, $total);    // 除法取平均数
        $last_number = bcsub($number, $divide_number * $total);    // 获取剩余
        $number_str = str_repeat($divide_number.'|', $total - $last_number);    // 拼装平分后的数据
        $number_str2 = str_repeat(($divide_number + 1).'|', $last_number);    // 拼装剩下的分配
        $number_str = $number_str2 . $number_str;    // 组合
        return explode('|', trim($number_str, '|')); // 去掉前后多余的分隔符 返回数组
    }


    /**
     * 生成指定长度的随机字符串
     * @param int $length 字符串长度
     * @param string $chars 生成字符范围
     * @return string
     */
    function random_string($length, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
        $result = '';
        $char_len = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $result .= $chars[rand(0, $char_len - 1)];
        }
        return $result;
    }
}