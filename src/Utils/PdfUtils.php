<?php

namespace yyctools\Utils;

use Dompdf\Dompdf;


class PdfUtils
{
    /**
     * 生成pdf
     * @param $html  html内容
     * @param $file_dir  文件保存目录
     * @param $filename  保存文件名
     * @return false|string
     */
    public static function create_contract_pdf($html,$file_dir,$filename = ""){
        try {
            if(empty($html)) return false;
            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            //portrait 纵向
            //landscape 横向
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            if (!is_dir($file_dir)){
                mkdir($file_dir,0777,true);
            }
            if(empty($filename)){
                $file_name = uniqid() . time();
                $newFile =  rtrim($file_dir,"/").'/'.$file_name. ".pdf" ;
            }else{
                $newFile =  rtrim($file_dir,"/").'/'.$filename;
            }
            file_put_contents($newFile,$dompdf->output());
            if(!file_exists($newFile)) return false;
            return ["file_path" => $newFile,"filename" => $filename];
        }catch (\Exception $exception){
            return false;
        }

    }
}