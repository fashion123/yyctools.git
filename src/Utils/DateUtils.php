<?php

namespace yyctools\Utils;

use Carbon\Carbon;
use DateInterval;
use DateTime;
use DateTimeZone;

class DateUtils
{
    /**
     * 根据日期获取周几
     * @param $date 日期
     * @return string
     */
    public static function get_week_day($date){
        $week_array = ["日", "一", "二", "三", "四", "五", "六"];
        $week = date("w", strtotime($date));
        return $week_array[$week];
    }

    /**
     * 获取月份天数
     * @param $month 月份
     * @param $format 格式
     * @param $dateTimeZone 时区
     * @return array
     * @throws \Exception
     */
    public static function get_month_days($month = "this month", $format = "Y-m-d", $dateTimeZone = false) {
        if(!$dateTimeZone) $dateTimeZone = new DateTimeZone("Asia/Shanghai");
        $start = new DateTime("first day of $month", $dateTimeZone);
        $end = new DateTime("last day of $month", $dateTimeZone);

        $days = array();
        for($time = $start; $time <= $end; $time = $time->modify("+1 day")) {
            $days[] = $time->format($format);
        }
        return $days;
    }

    /**
     *   获取 两个月份之前的所有 年月 数据 $start_time 2020-01 $en_time 2022-01
     * @param $start_time 开始时间
     * @param $end_time  结束时间
     * @return array
     */
    public static function get_two_date_all_month_info($start_time,$end_time){
        $start_year=substr($start_time,0,4);
        $start_month=substr($start_time,5,2);
        $end_year=substr($end_time,0,4);
        $end_month=substr($end_time,5,2);

        $all_date=[];
        $all_date[]=$start_year.$start_month;
        $final_data=$end_year.$end_month;

        //最多一百年
        $currency_year=$start_year;
        $currency_month=$start_month;
        for($i=0;$i<1200;$i++){
            $next_year_info=self::get_next_year_and_month($currency_year,$currency_month);
            if($next_year_info>$final_data)break;
            $all_date[]=$next_year_info;
            $currency_year=substr($next_year_info,0,4);
            $currency_month=substr($next_year_info,4,6);
        }

        return $all_date;
    }



    /**
     * 获取下一个年月
     * @param $year  年
     * @param $month 月
     * @return string
     */
    public static function get_next_year_and_month($year,$month){
        $year=(int)$year;
        $month=(int)$month;
        if($month==12)return ($year+1)."01";
        if($month<9)return $year."0".($month+1);
        return $year.($month+1);
    }

    /**
     * 根据生日计算年龄
     * @param $birthday 生日日期
     * @return int|mixed|string
     */
    public static function get_age($birthday) {
        if(!$birthday) return "";
        $age = 0;
        $year = $month = $day = 0;
        if (is_array($birthday)) {
            extract($birthday);
        } else {
            if (strpos($birthday, '-') !== false) {
                list($year, $month, $day) = explode('-', $birthday);
                $day = substr($day, 0, 2);
            }
        }
        $age = date('Y') - $year;
        if (date('m') < $month || (date('m') == $month && date('d') < $day)) $age--;
        return $age;
    }

    /**
     * 根据年龄获取日期
     * @param $age 年龄
     * @return mixed
     */
    public static function  get_birthday($age,$format = 'Y-m-d') {
        if(!$age) $age = 0;
        $now = new DateTime();
        $now->sub(new DateInterval("P" . $age . "Y"));
        return $now->format($format);
    }

    /**
     * 根据日期计算工龄(天数)
     * @param $start_date 开始日期
     * @param $end_date 结束日期
     * @param $leave_day 请假天数
     * @return int
     */
    public static function get_work_age($start_date,$end_date,$leave_day){
        $dt = Carbon::parse($start_date);
        $days = $dt->diffInDays($end_date);
        return floor(($days - $leave_day)) ;
    }

    /**
     * 计算请长假的天数
     * @param $start_date 开始日期
     * @param $end_date 结束日期
     * @return int
     */
    public static function get_diff_leave_day($start_date,$end_date){
        $dt = Carbon::parse($start_date);
        $days = $dt->diffInDays($end_date);
        return $days+1;
    }

    /**
     * 计算两个日期差
     * @param $start_date 开始日期
     * @param $end_date 结束日期
     * @return int|string
     * @throws \Exception
     */
    public static function calculate_diff_days($start_date,$end_date,$format = "%a"){
        if(!$start_date || !$end_date) return 0;
        $earlier = new DateTime($start_date);
        $later = new DateTime($end_date);
        $diff = $later->diff($earlier)->format($format);
        return $diff;
    }

    /**
     * 获取法定退休日期
     * @param $birth_date 生日日期
     * @param $sex 100男 200女
     * @param $sex_boy 男的退休年限
     * @param $sex_girl 女的退休年限
     * @return false|string|null
     */
    public static function get_legal_date($birth_date,$sex,$sex_boy=60,$sex_girl=55){
        $legal_date = null;
        if($sex == 100)   $legal_date = date("Y-m-d",strtotime("+".$sex_boy." year",strtotime($birth_date)));
        if($sex == 200)   $legal_date = date("Y-m-d",strtotime("+".$sex_girl." year",strtotime($birth_date)));
        return $legal_date;
    }

    /**
     * 获取两个时间之间的日期
     * @param $startDate 开始日期
     * @param $endDate 结束日期
     * @return array
     */
    public static function get_dates_between_two_days($startDate, $endDate)
    {
        $dates = [];
        if (strtotime($startDate) > strtotime($endDate)) {
            // 如果开始日期大于结束日期，直接return 防止下面的循环出现死循环
            return $dates;
        } elseif ($startDate == $endDate) {
            // 开始日期与结束日期是同一天时
            array_push($dates, $startDate);
            return $dates;
        } else {
            array_push($dates, $startDate);
            $currentDate = $startDate;
            do {
                $nextDate = date('Y-m-d', strtotime($currentDate . ' +1 days'));
                array_push($dates, $nextDate);
                $currentDate = $nextDate;
            } while ($endDate != $currentDate);

            return $dates;
        }
    }

}